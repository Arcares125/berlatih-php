<?php
function tukar_besar_kecil($string){
    $tmpung = "";

    for($i = 0; $i < strlen($string); $i++){
        $temp2 = ord($string[$i]);
        if($temp2 >= 65 && $temp2 <= 90){
            $plus = $temp2+32;
            $temp1 = chr($plus);
            $tmpung .= $temp1;
            
        } else if ($temp2 >= 90 && $temp2 < 122){
            $mines = $temp2-32;
            $temp1 = chr($mines);
            $tmpung .= $temp1;
        } else {
            $tmpung .= chr($temp2);
        }
    }
    return $tmpung . "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>