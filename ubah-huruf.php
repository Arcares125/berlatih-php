<?php
function ubah_huruf($string){
$word = "abcdefghijklmnopqrstuvwxyz";
$temp = "";
for($i = 0; $i < strlen($string); $i++){
    $place = strpos($word, $string[$i]);
    $temp .= substr($word, $place+1, 1);
    }   
    return $temp . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>

